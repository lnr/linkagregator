(function(){
	
	var agregator;

	var MotoLinkAgregator = function(optoins) {

		var domain = '',
			linksToCheck = {
				'data': {},
				'shift': function() {
					for(var i in this.data){
						var elem = this.data[i];
						delete this.data[i]
						return elem;
					}
				},
				'push': function(key, value) {
					this.data[key] = value;
				},
				'has': function(key) {
					return !!this.data[key];
				}
			},
			pause = false,
			$result,
			$broken,
			$counter,
			countLinks = 0,
			countBrokenLinks = 0,
			linkPattern = /<a.*?\shref="(?!mailto:)(.+?)".*?>.*?<\/a>/gm,
			imagePattern = /(.*?)(\.jpg|\.png|\.jpeg)$/i
			internalPattern = /^\/[a-zA-Z0-9]+/,
			titlePattern = /<title>\s*(.*?)\s*<\/title>/im,
			commentPattern = /<!--(.*?)-->/igm,
			result = {},
			brokenLinks = {};

		this.init = function(){
			domain = getDomain(document.URL);
			agregate(getLinks(document.getElementsByTagName('body')[0].innerHTML), 'root');
			renderHtml();
		};
		
		this.run = function() { pause = false; initWorker(linksToCheck.shift()); };

		this.pause = function() { pause = true; };
		
		var agregate = function(links, parentUrl) {
			links.map(function(link) {
				if( !linksToCheck.has( link.href ) && !result[link.href] && !isExternalLink(link.href)) {
					linksToCheck.push(link.href, {'url': link.href, 'parentUrl': parentUrl});
				}
			});
		};

		var renderHtml = function() {
			var box = document.createElement("div");

			box.id = 'moto_link_agregator_container';
			box.innerHTML = '<textarea id="moto_link_agregator_result"></textarea>'
							+'<table id="moto_link_agregator_broken"><tr><th>#</th><th>parentUrl</th><th>URL</th><th>STATE</th><th>MESSAGE</th></tr></table>'
							+'<div class="moto_counter">count: <div id="moto_link_counter"></div></div>';
			document.getElementsByTagName('body')[0].appendChild(box);
			$result = document.getElementById('moto_link_agregator_result');
			$broken = document.getElementById('moto_link_agregator_broken');
			$counter = document.getElementById('moto_link_counter');
		};

		var initWorker = function() {
			var link = linksToCheck.shift();
			if(!link)
			if (!link || pause) {
				if (!link) alert('done');
				return;
			}
			ajax({
				'url': link.url,
				'success': function(data, status, obj){

					var title = titlePattern.exec(data);
					result[link.url] = (title) ? title[1] : 'none';
					
					agregate(getLinks(data), link.url);
					
					$result.value += '<a href="'+link.url+'">' + result[link.url] + '</a>\n';
					$counter.innerText = ++countLinks;

					initWorker();

				},
				'error': function(resp, status, message){
					result[link.url] = 'error';
					addRow($broken, '<td>' + ++countBrokenLinks + '</td><td>' + link.parentUrl + '</td><td>' + link.url + '</td><td>' + resp.status + '</td><td>' + message + '</td>');
					initWorker();
				}
			});
		};

		var addRow = function(container, html) {
			var row = document.createElement('tr');
			row.innerHTML = html;
			container.appendChild(row);
		};

		var ajax = function(obj) {
			var request = new XMLHttpRequest();
			request.open('GET', obj.url, true);
			request.onload = function (e) {
				if (request.readyState === 4) {
					if (request.status === 200) {
						obj.success(request.responseText, request.status, request);
					} else {
						obj.error(request, request.status, request.statusText);
					}
				}
			};
			request.onerror = function (e) {
			    obj.error(request, request.status, request.statusText);
			};
			request.send(null);
		}

		var getLinks = function(text) {
			var link, str, res = [];
			while (str = linkPattern.exec(text)) {
				link = document.createElement('a');
				str[1] = str[1].split('#')[0];
				if(imagePattern.test(str[1])){
					continue;
				}
				link.href = (str[1].indexOf(domain) == 0) ? 'http://' + str[1] : str[1];
				link.innerText = str[2];
				res.push(link);
			}
			return res;
		};

		var getDomain = function(url) {
			var loc = document.createElement('a');
			loc.href = url;
			return loc.hostname;
		};

		var log = function(q) { if (console) { console.log(q); } };

		var isExternalLink = function(link) {
			return !(link.indexOf(domain) != -1 || internalPattern.test(link));
		};
	};

	chrome.runtime.onMessage.addListener(function(message, obj, callback){
		if( message === 'init' ) {
			if (!agregator) {
				agregator = new MotoLinkAgregator();
				agregator.init();
			}
		} else if (message === 'run') {
			agregator.run();
		} else if (message === 'pause') {
			agregator.pause();
		}
		
	});
})();
