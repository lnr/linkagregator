chrome.tabs.query({'active': true, 'currentWindow': true}, function(tabs) {

	var activeTab = tabs[0],
		container = document.getElementsByTagName('body')[0],
		btnStart,
		btnPause;

	chrome.tabs.sendMessage(activeTab.id, 'init');

	container.innerHTML = '<input type="button" value="start" id="start"><input type="button" id="pause" value="pause">';

	btnStart = document.getElementById('start');
	btnPause = document.getElementById('pause');
	
	btnStart.onclick = function() {
		chrome.tabs.sendMessage(activeTab.id, 'run');
	};

	btnPause.onclick = function() {
		chrome.tabs.sendMessage(activeTab.id, 'pause');
	};

});